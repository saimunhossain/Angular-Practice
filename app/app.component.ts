import {Component} from 'angular2/core';
import {AuthorsComponent} from './authors.component';
import {CoursesComponent} from './courses.component';

@Component({
    selector: 'my-app',
    template: '<h1>My First Angular 2 App</h1><authors></authors><courses></courses>'
    directives: [AuthorsComponent, CoursesComponent]
})
export class AppComponent { }
